%Short script to generate the window function output
N_RP = 32;
N_CP = 96;
N_FFT = 256;
outLength = N_RP + N_CP + N_FFT;

W = zeros(outLength,1);
for index=1:outLength
    if index<=N_RP
        W(index) = 1/2*(1-sin(pi/N_RP*((index-1)-(N_FFT+N_CP)/2+1/2)));
    elseif index>=(outLength-N_RP)
        W(index) = 1/2*(1-sin(pi/N_RP*((index-1)-(N_FFT+N_CP)/2+1/2)));
    else
        W(index) = 1;
    end
end

% Create hex output
A = fi(W,1,18,16);  %Signed output, with 18 bit length and 16 fractional bits
out = hex(A);   %Create the hex output vector
