`timescale 1ns / 1ps

module RCNT_tb();
  reg sym_clk, tx_clock, reset, source_valid;
  wire [8:0] ram_write_addr, ram_read_addr, ram_read_counter;
  
  initial
  begin
      sym_clk = 0;
      tx_clock = 0;
      reset = 0;
      source_valid = 0;
  end
  
  initial
    #30 reset = 1;
  initial
    #30 source_valid = 1;

  
  always
    #7.5  sym_clk =! sym_clk;
    
  always
    #5 tx_clock =! tx_clock;
  
  initial
    #10000 $stop;
  
  RCNT RCNT_1(  .sym_clk         (sym_clk),
                .tx_clock        (tx_clock),
                .reset           (reset),
                .source_valid    (source_valid),
                .ram_write_addr  (ram_write_addr),
                .ram_read_addr   (ram_read_addr),
                .ram_read_counter(ram_read_counter)
            );
  
endmodule