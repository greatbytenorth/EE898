vsim -L altera_ver -L stratixiv_ver -gui work.RCNT_tb
onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /RCNT_tb/sym_clk
add wave -noupdate /RCNT_tb/tx_clock
add wave -noupdate /RCNT_tb/reset
add wave -noupdate /RCNT_tb/source_valid
add wave -noupdate /RCNT_tb/ram_write_addr
add wave -noupdate /RCNT_tb/ram_read_addr
add wave -noupdate /RCNT_tb/ram_read_counter
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {1 ns}
run -all