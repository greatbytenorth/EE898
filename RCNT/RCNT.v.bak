module RCNT(  	sym_clk,
				tx_clock,
				reset,
				source_valid,
				ram_write_addr,
				ram_read_addr,
				ram_read_counter
            );
input sym_clk, tx_clock, reset, source_valid;
output [8:0] ram_read_addr, ram_write_addr, ram_read_counter;

//wire sym_clk, tx_clock, reset, source_valid;
//wire [8:0] ram_read_addr;

reg [7:0] ram_read_addr_low;
reg [8:0] ram_write_addr, ram_read_counter, ram_read_addr;

/********************************** Ram Write Counter ****************************
Keeps track of current read and write position of the ram. 
It follows the fft output, not the input symbol stream.
*********************************************************************************/
always @ (posedge sym_clk)
	if (reset == 1'b0 || source_valid == 1'b0)
		ram_write_addr = 9'd0;
	else
		ram_write_addr = ram_write_addr + 9'd1;
		
/*********************************Ram Read Counter *******************************
 This function will be running at 1.5* the speed and will create the output
 for the reading of the ram.  We also only count to 383 because we are counting
 starting at 0.
*********************************************************************************/

always @ (posedge tx_clock)
	if (reset == 1'b0 || source_valid == 1'b0 || ram_read_counter == 9'd383 )	
		begin
			ram_read_counter = 9'd0;
			ram_read_addr = {~ram_write_addr[8], 9'd160};
		end
	else
		begin
			ram_read_counter = ram_read_counter + 9'd1;
			ram_read_addr = {~ram_write_addr[8], ram_read_addr[7:0]+8'd1};
		end

endmodule
