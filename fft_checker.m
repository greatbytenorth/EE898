length_input = length(FFT_in);
closeness = zeros(1,length_input);
for index = 1 : length_input-256
    temp = ifft(FFT_in(index:index+255))/sqrt(256);
    closeness(index) = sum(abs(FFT_out-temp));
end