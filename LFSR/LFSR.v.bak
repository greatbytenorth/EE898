module LFSR (load_enable, clk, q_out);
input load_enable, clk;
output q_out;
  
/*  Creates a 16 bit random number generator using a Linear
    feedback shift registers
    
    load_data:  initial data to place into LFSR
    load_enable:  Allows data to be loaded into the LFSR
    clk:        Clock source, each clock edge generates a random bit
    q_out:      Output bit
*/

//  q is shift register
reg [15:0] q;
wire d0;

assign q_out = q[0];

// Shift q unless we want to reset it (i.e. load it with 16'hFFFF)
always @ (posedge clk)
  if (load_enable == 1'b1)
    q = 16'HFFFF;
  else
    q = {q[14:0], d0};
  
//Feedback network
assign d0 = (~q[1] || ~( ~q[2] || ~(~q[15] || ~q[10])));
  
endmodule