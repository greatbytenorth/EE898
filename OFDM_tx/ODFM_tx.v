module OFDM_tx(	clk, 
						reset, 
						sym_clk,
						tx_clock,
						sym_count_out, 
						sink_real, 
						sink_imag, 
						sink_sop, 
						sink_eop, 
						source_sop, 
						source_eop, 
						source_valid,
						source_real, 
						source_imag, 
						source_ready, 
						sink_error, 
						source_error, 
						sink_ready,
						ifft_imag, 
						ifft_real,
						ram_read_addr,
						ram_write_addr,
						ram_read_counter
						);
  input  clk, reset;
  output sym_clk, sink_sop, sink_eop, source_sop, source_eop, source_valid, source_ready, sink_ready, tx_clock;
  output [1:0] sink_error, source_error;
  output [7:0] sym_count_out;
  output [8:0] ram_read_addr, ram_write_addr, ram_read_counter;
  output [17:0] sink_real, sink_imag, source_real, source_imag, ifft_imag, ifft_real;
  
  reg sym_clk = 1'b1;
  reg sink_valid, sink_sop, sink_eop;
  reg [1:0] symbol, symbuffer;
  reg [2:0] tx_counter_steps;
  reg [5:0] window_addr;
  reg [8:0] sym_count;
  reg signed [17:0] sink_real, sink_imag;
  
  wire q_out, reset, load_enable, sink_ready, source_sop, source_eop, source_ready, source_valid;
  wire fft_inverse, tx_clock, clk_read, clk_write, window_clk;
  wire [1:0] source_error, sink_error;
  wire [7:0] sym_count_out;
  wire [8:0] ram_read_addr, ram_write_addr, ram_read_counter;
  wire signed [17:0] source_real, source_imag, ifft_real, ifft_imag, window;
  
  
  //*****************Wire assignments***************************
  assign load_enable = ~reset;				// Random bit generator uses active high reset
  assign sym_count_out = sym_count[7:0];	// Only send out the first 8 bits
  assign source_ready = 1'b1;					// The source is always ready
  assign fft_inverse = 1'b1;					// Set fft to perform the inverse
  
  //***************** Symbol Buffer ***********************
  
  // Store random bits, only need to store 2 at a time
  always @ (posedge clk)
    if (reset == 1'b0)
      symbuffer = 2'b00;
    else
      symbuffer = {symbuffer[0],q_out};
  
  // Store each symbol, each symbol being two bits and a new one happening each 2 clk cycles
  always @ (posedge sym_clk)
    if (reset == 1'b0)
      symbol = 2'b00;
    else
      symbol = symbuffer;
  
  // ******************* Symbol Counter **********************
  // Count the number of bits, noting when we fill the buffer and when resets occur
  // Note that counting is a little weird.  Starts at 511 beccuase I wanted to index
  // at 0, so the overflow brings it to 0 at the start.  It then counts to 254, because
  // we actually shift data on 255 and the next one after this will be 0.
  always @(posedge sym_clk)
  begin
    if (reset == 1'b0 || sink_ready == 1'b0)
      begin
        sym_count = 9'd511;
        sink_eop = 1'b0;  // Reset the end of packet
        sink_sop = 1'b0;  // Reset the start of packet to 1 (cause we are there duuuude)
      end
    else if (sym_count == 9'd254)  //Check for overflow
      begin
        sym_count = 9'd511;
        sink_eop = 1'b1;
      end
    else
      begin
        sym_count = sym_count + 9'd1;
        sink_eop = 1'b0;
        if (sym_count == 9'd0)
          sink_sop = 1'b1;
        else
          sink_sop = 1'b0;
      end
  end
  
  // Create sink valid signal
  always @ *
	if (reset == 1'b0)
		sink_valid = 1'b0;
	else
		sink_valid = 1'b1;
			
  //******************  Binary to Signed conversion *********************   
  
  //Change the binary values into their 18bit representations for the FFT
  always @ *
	if (symbol[0] == 1'b1)
		sink_real = -18'H1_0000;
	else
		sink_real = 18'H1_0000;
		
	//Change the binary values into their 18bit representations for the FFT
  always @ *
	if (symbol[1] == 1'b1)
		sink_imag = -18'H1_0000;
	else
		sink_imag = 18'H1_0000;
  
  //********************CLOCKs***************************
  // Generate a clock source
  always @ (negedge clk)
    sym_clk = ~sym_clk;
  //Set the write clock to the same rate as the sym clock    
  assign clk_write = sym_clk;
  //Set the read clock to the same rate as the sym clock (temporary)
  assign clk_read = sym_clk;
  
  //****************  MODULES *********************
  
  //Add the random number generator
  LFSR LFSR_1(
        .load_enable  (load_enable), 
        .clk          (clk), 
        .q_out        (q_out)
        );
	
	IFFT IFFT_1(
			.clk				(sym_clk),
			.reset_n			(reset),
			.inverse			(fft_inverse),
			.sink_valid		(sink_valid),
			.sink_sop		(sink_sop),
			.sink_eop		(sink_eop),
			.sink_real		(sink_real),
			.sink_imag		(sink_imag),
			.sink_error		(sink_error),
			.source_ready	(source_ready),
			.sink_ready		(sink_ready),
			.source_error	(source_error),
			.source_sop		(source_sop),
			.source_eop		(source_eop),
			.source_valid	(source_valid),
			.source_real	(source_real),
			.source_imag	(source_imag)
			);
			
	// 2port ram for real components
	tx_ram real_ram(
			.address_a		(ram_write_addr),
			.address_b		(ram_read_addr),
			.clock_a			(clk_write),
			.clock_b			(tx_clock),
			.data_a			(source_real),
			.wren_a			(source_valid),	//Only write to ram when data is valid
			.q_b				(ifft_real)
			);
	
	// 2port ram for imaginary components
	tx_ram imag_ram(
			.address_a		(ram_write_addr),
			.address_b		(ram_read_addr),
			.clock_a			(clk_write),
			.clock_b			(tx_clock),
			.data_a			(source_imag),
			.wren_a			(source_valid),	//Only write to ram when data is valid
			.q_b				(ifft_imag)
			);
  
	// PLL to create the transmit clock
	tx_clk tx_clk1(
			.inclk0			(clk),
			.c0				(tx_clock)
			);
			
	//Ram counter module
	RCNT RCNT_1		(
						.sym_clk				(sym_clk),
						.tx_clock			(tx_clock),
						.reset				(reset),
						.source_valid		(source_valid),
						.ram_write_addr	(ram_write_addr),
						.ram_read_addr		(ram_read_addr),
						.ram_read_counter	(ram_read_counter)
						);
	// Window ROM					
	window_ROM WR1		(
							.address		(window_addr),
							.clock		(window_clk),
							.q				(window)
							);

endmodule