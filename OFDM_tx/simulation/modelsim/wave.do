vsim  -L altera_ver -L stratixiv_ver -gui work.OFDM_tx_tb
onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /OFDM_tx_tb/clk
add wave -noupdate /OFDM_tx_tb/reset
add wave -noupdate /OFDM_tx_tb/sym_clk
add wave -noupdate /OFDM_tx_tb/tx_clock
add wave -noupdate /OFDM_tx_tb/sink_sop
add wave -noupdate /OFDM_tx_tb/sink_eop
add wave -noupdate /OFDM_tx_tb/sink_ready
add wave -noupdate /OFDM_tx_tb/source_sop
add wave -noupdate /OFDM_tx_tb/source_eop
add wave -noupdate /OFDM_tx_tb/source_valid
add wave -noupdate /OFDM_tx_tb/source_ready
add wave -noupdate /OFDM_tx_tb/source_error
add wave -noupdate /OFDM_tx_tb/sink_error
add wave -noupdate /OFDM_tx_tb/sym_count
add wave -noupdate /OFDM_tx_tb/sink_real
add wave -noupdate /OFDM_tx_tb/sink_imag
add wave -noupdate /OFDM_tx_tb/source_real
add wave -noupdate /OFDM_tx_tb/source_imag
add wave -noupdate /OFDM_tx_tb/ram_read_counter
add wave -noupdate /OFDM_tx_tb/ram_read_addr
add wave -noupdate /OFDM_tx_tb/ram_write_addr
add wave -noupdate /OFDM_tx_tb/ifft_imag
add wave -noupdate /OFDM_tx_tb/ifft_real

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1682258 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {6300 ns}

add list  \
sim:/OFDM_tx_tb/sink_real \
sim:/OFDM_tx_tb/sink_imag \
sim:/OFDM_tx_tb/source_real \
sim:/OFDM_tx_tb/source_imag

run -all