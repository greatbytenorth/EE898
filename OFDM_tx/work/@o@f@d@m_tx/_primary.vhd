library verilog;
use verilog.vl_types.all;
entity OFDM_tx is
    port(
        clk             : in     vl_logic;
        reset           : in     vl_logic;
        sym_clk         : out    vl_logic;
        symbol          : out    vl_logic_vector(1 downto 0);
        sym_count_out   : out    vl_logic_vector(7 downto 0);
        sink_real       : out    vl_logic_vector(17 downto 0);
        sink_imag       : out    vl_logic_vector(17 downto 0);
        sink_sop        : out    vl_logic;
        sink_eop        : out    vl_logic;
        source_sop      : out    vl_logic;
        source_eop      : out    vl_logic;
        source_valid    : out    vl_logic;
        source_real     : out    vl_logic_vector(17 downto 0);
        source_imag     : out    vl_logic_vector(17 downto 0)
    );
end OFDM_tx;
