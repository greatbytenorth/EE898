module OFDM_tx(clk, reset, sym_clk, symbol, sym_count_out, 
                sink_real, sink_imag, sink_sop, sink_eop, source_sop, source_eop, source_valid,
                source_real, source_imag, source_ready, sink_valid, sink_error, source_error, sink_ready,
                fft_inverse);
  input clk, reset;
  output sym_clk, sink_sop, sink_eop, source_sop, source_eop, source_valid, source_ready, sink_valid, sink_ready;
  output fft_inverse;
  output [1:0] symbol, sink_error, source_error;
  output [7:0] sym_count_out;
  output [17:0] sink_real, sink_imag, source_real, source_imag;
  
  reg sym_clk = 1'b1;
  reg sink_valid, sink_sop, sink_eop;
  reg [1:0] symbol, symbuffer;
  reg [8:0] sym_count;
  reg signed [17:0] sink_real, sink_imag;
  
  wire q_out, reset, load_enable, sink_ready, source_sop, source_eop, source_ready, source_valid;
  wire fft_inverse;
  wire [7:0] sym_count_out;
  wire [17:0] source_real, source_imag;
  wire [1:0] source_error, sink_error;
  
  assign load_enable = ~reset;
  assign sym_count_out = sym_count[7:0];
  assign source_ready = 1'b1;
  assign fft_inverse = 1'b1;
  
  // Store random bits, only need to store 2 at a time
  always @ (posedge clk)
    if (reset == 1'b0)
      symbuffer = 2'b00;
    else
      symbuffer = {symbuffer[0],q_out};
  
  // Store each symbol, each symbol being two bits and a new one happening each 2 clk cycles
  always @ (posedge sym_clk)
    if (reset == 1'b0)
      symbol = 2'b00;
    else
      symbol = symbuffer;
  
  // ******************* Symbol Counter **********************
  // Count the number of bits, noting when we fill the buffer and when resets occur
  // Note that counting is a little weird.  Starts at 511 beccuase I wanted to index
  // at 0, so the overflow brings it to 0 at the start.  It then counts to 254, because
  // we actually shift data on 255 and the next one after this will be 0.
  always @(posedge sym_clk)
  begin
    if (reset == 1'b0 || sink_ready == 1'b0)
      begin
        sym_count = 9'd511;
        sink_eop = 1'b0;  // Reset the end of packet
        sink_sop = 1'b0;  // Reset the start of packet to 1 (cause we are there duuuude)
      end
    else if (sym_count == 9'd254)  //Check for overflow
      begin
        sym_count = 9'd511;
        sink_eop = 1'b1;
      end
    else
      begin
        sym_count = sym_count + 9'd1;
        sink_eop = 1'b0;
        if (sym_count == 9'd0)
          sink_sop = 1'b1;
        else
          sink_sop = 1'b0;
      end
  end
  
  always @ *
	if (reset == 1'b0)
		sink_valid = 1'b0;
	else
		sink_valid = 1'b1;
  //******************  Clocks and Simple Conversions *********************   
  
  //Change the binary values into their 18bit representations for the FFT
  always @ *
	if (symbol[0] == 1'b1)
		sink_real = -18'H1_0000;
	else
		sink_real = 18'H1_0000;
		
	//Change the binary values into their 18bit representations for the FFT
  always @ *
	if (symbol[1] == 1'b1)
		sink_imag = -18'H1_0000;
	else
		sink_imag = 18'H1_0000;
  
  // Generate a clock source
  always @ (negedge clk)
    sym_clk = ~sym_clk;
      
      
      
  //****************  MODULES *********************
  
  //Add the random number generator
  LFSR LFSR_1(
        .load_enable  (load_enable), 
        .clk          (clk), 
        .q_out        (q_out)
        );
	
	IFFT IFFT_1(
			.clk				(sym_clk),
			.reset_n			(reset),
			.inverse			(fft_inverse),
			.sink_valid		(sink_valid),
			.sink_sop		(sink_sop),
			.sink_eop		(sink_sop),
			.sink_real		(sink_real),
			.sink_imag		(sink_imag),
			.sink_error		(sink_error),
			.source_ready	(source_ready),
			.sink_ready		(sink_ready),
			.source_error	(source_error),
			.source_sop		(source_sop),
			.source_eop		(source_eop),
			.source_valid	(source_valid),
			.source_exp		(),
			.source_real	(source_real),
			.source_imag	(source_imag)
			);
  
endmodule